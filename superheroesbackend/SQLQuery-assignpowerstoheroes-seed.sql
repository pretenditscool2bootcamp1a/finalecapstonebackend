﻿INSERT INTO superhero_superpower (superhero_id, superpower_id)
VALUES
  (1, 3),    -- Thunderstrike -> Super Strength
  (1, 32),   -- Thunderstrike -> Electricity Control
  (2, 2),    -- Shadowdancer -> Invisibility
  (2, 9),    -- Shadowdancer -> Teleportation
  (3, 11),   -- Magma Blaze -> Elemental Control
  (3, 26),   -- Magma Blaze -> Molecular Manipulation
  (4, 12),   -- Frostbite -> Cryokinesis
  (4, 15),   -- Frostbite -> Healing
  (5, 18),   -- Gravity Shift -> Gravity Control
  (5, 31),   -- Gravity Shift -> Force Field Generation
  (6, 1),    -- Nova -> Flight
  (6, 6);    -- Nova -> Elemental Control
