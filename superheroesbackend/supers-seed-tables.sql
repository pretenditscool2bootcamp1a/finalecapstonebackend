﻿INSERT INTO superpower (name, description, category)
VALUES
    ('Super Strength', 'Ability to exert great physical force', 'Offensive'),
    ('Telekinesis', 'Ability to move objects with the mind', 'Control'),
    ('Cryokinesis', 'Ability to control ice and create extreme cold', 'Offensive'),
    ('Fire Breath', 'Ability to exhale flames from the mouth', 'Offensive'),
    ('Invisibility', 'Ability to render oneself unseen', 'Control'),
    ('Elasticity', 'Ability to stretch and deform the body', 'Movement');


INSERT INTO superhero (name, gender, secret_identity, date_of_birth, home_location, current_location, costume_description, weakness, image_path, inclusive_age, created_at, updated_at)
VALUES
    ('The Crimson Cyclone', 'Male', 'Clifford "Cliff" Steele', '1940-03-01', 'Central City', 'Gotham City', 'A red suit with a yellow cape and a lightning bolt emblem', 'Water', 'img/crimson-cyclone.png', 'an elder statesman', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678'),
    ('Mighty Lightning Boy', 'Non-Binary', 'Charlie Rayne', '1998-06-15', 'New York City', 'Los Angeles', 'A yellow and black jumpsuit with a lightning bolt emblem', 'Loud Noises', 'img/lightning-boy.png', 'in their prime', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678'),
    ('The Quantum Queen', 'Female', 'Daphne Dean', '1980-09-25', 'Seattle', 'Vancouver', 'A black jumpsuit with a silver quantum symbol on the chest', 'Magnetism', 'img/quantum-queen.png', 'a woman in her thirties', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678'),
    ('The Sapphire Sorceress', 'Female', 'Lila Blake', '1995-11-03', 'Chicago', 'Miami', 'A blue and silver robe with a sapphire pendant', 'Iron', 'img/sapphire-sorceress.png', 'a woman in her twenties', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678'),
    ('The Terrific Titan', 'Male', 'Michael Holt', '1970-06-01', 'Coast City', 'Star City', 'A black and white suit with a T emblem on the chest', 'Radiation', 'img/terrific-titan.png', 'a man in his fifties', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678'),
    ('The Silver Serpent', 'Non-Binary', 'Lian Harper', '2000-01-17', 'San Francisco', 'Portland', 'A silver and green bodysuit with a serpent emblem on the chest', 'Electricity', 'img/silver-serpent.png', 'in their prime', '2022-04-13 01:23:45.678', '2022-04-13 01:23:45.678');

INSERT INTO superhero_superpower (superhero_id, superpower_id)
VALUES
    (1, 1), (1, 2), (2, 1), (2, 5), (3, 2), (3, 4), (4, 2), (4, 5), (4, 6), (5, 1), (5, 3), (6, 1), (6, 2), (6, 5);
