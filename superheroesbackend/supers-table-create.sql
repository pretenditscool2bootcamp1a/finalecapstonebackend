﻿CREATE TABLE superhero (
    SuperheroId INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    gender VARCHAR(20) NULL,
    secret_identity VARCHAR(255) NULL,
    date_of_birth DATE NULL,
    home_location VARCHAR(255) NULL,
    current_location VARCHAR(255) NULL,
    costume_description VARCHAR(255) NULL,
    weakness VARCHAR(255) NULL,
    image_path VARCHAR(255) NULL,
    inclusive_age VARCHAR(255) NULL,
    created_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME(),
    updated_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME()
);

CREATE TABLE superpower (
    SuperpowerId INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    category VARCHAR(50) NULL,
    created_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME(),
    updated_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME()
);

CREATE TABLE superhero_superpower (
    SuperheroId INT NOT NULL,
    SuperpowerId INT NOT NULL,
    PRIMARY KEY (SuperheroId, SuperpowerId),
    FOREIGN KEY (SuperheroId) REFERENCES superhero(SuperheroId) ON DELETE CASCADE,
    FOREIGN KEY (superpowerId) REFERENCES superpower(SuperpowerId) ON DELETE CASCADE,
    created_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME(),
    updated_at DATETIME2(3) NOT NULL DEFAULT SYSUTCDATETIME()
);
