﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using superheroesbackend.Models;
using superheroesbackend.Repository;
using Microsoft.AspNetCore.Cors;

namespace superheroesbackend.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class SuperheroesController : ControllerBase
    {
        private readonly ISuperheroRepository _repository;

        public SuperheroesController(ISuperheroRepository repository)
        {
            _repository = repository;
        }

        // GET api/superheroes
        [HttpGet]
        public ActionResult<IEnumerable<Superhero>> GetAllSuperheroes()
        {
            var superheroes = _repository.GetAllHeroes();
            return Ok(superheroes);
        }

        // GET api/superheroes/5
        [HttpGet("{id}")]
        public ActionResult<Superhero> GetSuperheroById(int id)
        {
            var superhero = _repository.GetHeroById(id);

            if (superhero == null)
            {
                return NotFound();
            }

            return Ok(superhero);
        }

        // POST api/superheroes
        [HttpPost]
        public ActionResult<SuperheroDTO> AddSuperhero(SuperheroDTO superheroDTO)
        {
            var superhero = SuperheroDTO.ToSuperhero(superheroDTO);
            _repository.AddSuperhero(superheroDTO);

            return CreatedAtAction(nameof(GetSuperheroById), new { id = superhero.SuperheroId }, SuperheroDTO.FromSuperhero(superhero));
        }



        // PUT api/superheroes/5
        [HttpPut("{id}")]
        public IActionResult UpdateSuperhero(int id, SuperheroDTO superheroDTO)
        {
            if (id != superheroDTO.SuperheroId)
            {
                return BadRequest();
            }

            _repository.UpdateSuperhero(superheroDTO);

            return NoContent();
        }

        // DELETE api/superheroes/5
        [HttpDelete("{id}")]
        public IActionResult DeleteSuperhero(int id)
        {
            var superhero = _repository.GetHeroById(id);

            if (superhero == null)
            {
                return NotFound();
            }

            _repository.DeleteSuperhero(id);

            return NoContent();
        }

        // GET api/superheroes/search?superpower=fire
        [HttpGet("search")]
        public ActionResult<IEnumerable<Superhero>> FindSuperheroesBySuperpower(string superpower)
        {
            var matchingSuperheroes = _repository.GetAllHeroes()
                .Where(s => s.Superpowers.Any(sp => sp.Name.Contains(superpower, StringComparison.OrdinalIgnoreCase)))
                .ToList();

            return Ok(matchingSuperheroes);
        }
    }
}
