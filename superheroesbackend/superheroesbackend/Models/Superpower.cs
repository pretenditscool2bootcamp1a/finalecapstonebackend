﻿using System;
using System.Collections.Generic;

namespace superheroesbackend.Models;

public class Superpower
{
    public int SuperpowerId { get; set; }
    public string? Name { get; set; }
    public string? Category { get; set; }
    public string? Description { get; set; }

    public virtual ICollection<SuperheroSuperpower>? SuperheroSuperpowers { get; set; }
    public virtual ICollection<Superhero>? Superheroes { get; set; }
}
