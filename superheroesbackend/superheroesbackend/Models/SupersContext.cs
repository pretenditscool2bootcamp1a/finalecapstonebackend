﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace superheroesbackend.Models
{
    public partial class SupersContext : DbContext
    {
        public SupersContext()
        {
        }

        public SupersContext(DbContextOptions<SupersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Superhero> Superheroes { get; set; }
        public virtual DbSet<SuperheroSuperpower> SuperheroSuperpowers { get; set; }
        public virtual DbSet<Superpower> Superpowers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=supers;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Superhero>(entity =>
            {
                entity.HasKey(e => e.SuperheroId);
                entity.ToTable("superhero");
                entity.Property(e => e.SuperheroId).HasColumnName("SuperheroId");
                entity.Property(e => e.CostumeDescription).HasColumnName("costume_description");
                entity.Property(e => e.CurrentLocation).HasColumnName("current_location");
                entity.Property(e => e.DateOfBirth).HasColumnName("date_of_birth");
                entity.Property(e => e.Gender).HasColumnName("gender");
                entity.Property(e => e.HomeLocation).HasColumnName("home_location");
                entity.Property(e => e.ImagePath).HasColumnName("image_path");
                entity.Property(e => e.InclusiveAge).HasColumnName("inclusive_age");
                entity.Property(e => e.Name).HasColumnName("name");
                entity.Property(e => e.SecretIdentity).HasColumnName("secret_identity");
                entity.Property(e => e.Weakness).HasColumnName("weakness");
            });

            modelBuilder.Entity<SuperheroSuperpower>(entity =>
            {
                entity.HasKey(e => new { e.SuperheroID, e.SuperpowerId });
                entity.ToTable("superhero_superpower");
                entity.Property(e => e.SuperheroID).HasColumnName("SuperheroID");
                entity.Property(e => e.SuperpowerId).HasColumnName("SuperpowerId");
            });

            modelBuilder.Entity<Superpower>(entity =>
            {
                entity.HasKey(e => e.SuperpowerId);
                entity.ToTable("superpower");
                entity.Property(e => e.SuperpowerId).HasColumnName("SuperpowerId");
                entity.Property(e => e.Category).HasColumnName("category");
                entity.Property(e => e.Description).HasColumnName("description");
                entity.Property(e => e.Name).HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
