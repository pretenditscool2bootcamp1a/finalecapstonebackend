﻿using System;
using System.Collections.Generic;

namespace superheroesbackend.Models;

public partial class SuperheroSuperpower
{
    public int SuperheroID { get; set; }

    public int SuperpowerId { get; set; }

    public virtual Superhero Superhero { get; set; } = null!;

    public virtual Superpower Superpower { get; set; } = null!;
}
