﻿using superheroesbackend.Repository;
using System;
using System.Collections.Generic;

namespace superheroesbackend.Models;

public class Superhero
{
    public int SuperheroId { get; set; }
    public string? Name { get; set; }
    public string? Gender { get; set; }
    public string? SecretIdentity { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string? HomeLocation { get; set; }
    public string? CurrentLocation { get; set; }
    public string? CostumeDescription { get; set; }
    public string? Weakness { get; set; }
    public string? ImagePath { get; set; }
    public string? InclusiveAge { get; set; }

    public virtual ICollection<SuperheroSuperpower>? SuperheroSuperpowers { get; set; }
    public virtual ICollection<Superpower>? Superpowers { get; set; }
    //public virtual List<SuperpowerDTO> Superpowers { get; set; }
}
