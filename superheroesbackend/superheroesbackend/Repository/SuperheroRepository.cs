﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using superheroesbackend.Models;
using superheroesbackend.Exceptions;

namespace superheroesbackend.Repository
{
    public class SuperheroRepository : ISuperheroRepository
    {
        private readonly SupersContext _context;

        public SuperheroRepository(SupersContext context)
        {
            _context = context;
        }

        public IEnumerable<SuperheroDTO> GetAllHeroes()
        {
            var superheroes = _context.Superheroes
            .Include(s => s.SuperheroSuperpowers)
            .ThenInclude(ss => ss.Superpower)
            .ToList();


            var superheroDTOs = superheroes.Select(s => new SuperheroDTO
            {
                SuperheroId = s.SuperheroId,
                Name = s.Name,
                Gender = s.Gender ?? "",
                SecretIdentity = s.SecretIdentity ?? "",
                DateOfBirth = s.DateOfBirth,
                HomeLocation = s.HomeLocation ?? "",
                CurrentLocation = s.CurrentLocation ?? "",
                CostumeDescription = s.CostumeDescription ?? "",
                Weakness = s.Weakness ?? "",
                ImagePath = s.ImagePath ?? "",
                InclusiveAge = s.InclusiveAge ?? "",
                Superpowers = s.SuperheroSuperpowers.Select(ss => new SuperpowerDTO
                {
                    SuperpowerId = ss.Superpower.SuperpowerId,
                    Name = ss.Superpower.Name ?? "",
                    Description = ss.Superpower.Description ?? "",
                    Category = ss.Superpower.Category ?? ""
                }).ToList()
            });

            return superheroDTOs;
        }



        public SuperheroDTO GetHeroById(int id)
        {
            var superhero = _context.Superheroes
                .AsNoTracking()
                .Include(s => s.SuperheroSuperpowers)
                .ThenInclude(ss => ss.Superpower)
                .FirstOrDefault(s => s.SuperheroId == id);

            if (superhero == null)
            {
                return null;
            }

            return new SuperheroDTO
            {
                SuperheroId = superhero.SuperheroId,
                Name = superhero.Name,
                Gender = superhero.Gender ?? "",
                SecretIdentity = superhero.SecretIdentity ?? "",
                DateOfBirth = superhero.DateOfBirth,
                HomeLocation = superhero.HomeLocation ?? "",
                CurrentLocation = superhero.CurrentLocation ?? "",
                CostumeDescription = superhero.CostumeDescription ?? "",
                Weakness = superhero.Weakness ?? "",
                ImagePath = superhero.ImagePath ?? "",
                InclusiveAge = superhero.InclusiveAge ?? "",
                Superpowers = superhero.SuperheroSuperpowers.Select(ss => new SuperpowerDTO
                {
                    SuperpowerId = ss.Superpower.SuperpowerId,
                    Name = ss.Superpower.Name ?? "",
                    Description = ss.Superpower.Description ?? "",
                    Category = ss.Superpower.Category ?? ""
                }).ToList()
            };
        }


        public void AddSuperhero(SuperheroDTO superheroDTO)
        {
            var superhero = SuperheroDTO.ToSuperhero(superheroDTO);
            _context.Superheroes.Add(superhero);
            _context.SaveChanges();
            var heroJustAdded = GetHeroById(superhero.SuperheroId); // not sure if i need this . i'm just trying to get the newly created superheroid 
            superheroDTO.SuperheroId = superhero.SuperheroId;
            UpdateSuperhero(superheroDTO);
/*            foreach (var spDTO in superheroDTO.Superpowers)
            {
               //SuperpowerDTO spDTOtemp = SuperpowerDTO.FromSuperpower(spDTO);
                var existingSuperpower = _context.Superpowers.FirstOrDefault(sp => sp.Name == spDTO.Name);
                if (existingSuperpower == null)
                {
                    Superpower newSuperpower = SuperpowerDTO.ToSuperpower(spDTO);
                    _context.Superpowers.Add(newSuperpower);
                    _context.SaveChanges();

                    SuperheroSuperpower ss = new SuperheroSuperpower
                    {
                        SuperheroID = superhero.SuperheroId,
                        SuperpowerId = newSuperpower.SuperpowerId
                    };
                    _context.SuperheroSuperpowers.Add(ss);
                    _context.SaveChanges();
                }
                else
                {
                    SuperheroSuperpower ss = new SuperheroSuperpower
                    {
                        SuperheroID = superhero.SuperheroId,
                        SuperpowerId = existingSuperpower.SuperpowerId
                    };
                    _context.SuperheroSuperpowers.Add(ss);
                    _context.SaveChanges();
                }
            }
*/
            
        }

        public void UpdateSuperhero(SuperheroDTO superheroDTO)
        {
            var superhero = _context.Superheroes.Include(sh => sh.SuperheroSuperpowers)
                                                  .ThenInclude(ss => ss.Superpower)
                                                  .FirstOrDefault(sh => sh.SuperheroId == superheroDTO.SuperheroId);

            if (superhero == null)
            {
                throw new NotFoundException("Superhero not found");
            }

            SuperheroDTO.SuperheroDTOtoSuperhero(superheroDTO, superhero);

            // Update superpowers
            var updatedSuperpowers = new List<Superpower>();
            foreach (var spDTO in superheroDTO.Superpowers)
            {
                var existingSuperpower = _context.Superpowers.FirstOrDefault(sp => sp.Name == spDTO.Name);
                if (existingSuperpower == null)
                {
                    // Create new superpower if it doesn't exist
                    var newSuperpower = SuperpowerDTO.ToSuperpower(spDTO);
                    _context.Superpowers.Add(newSuperpower);
                    updatedSuperpowers.Add(newSuperpower);
                }
                else
                {
                    updatedSuperpowers.Add(existingSuperpower);
                }
            }

            // Remove any superpowers that are not in the updated list
            foreach (var ss in superhero.SuperheroSuperpowers)
            {
                if (!updatedSuperpowers.Contains(ss.Superpower))
                {
                    superhero.SuperheroSuperpowers.Remove(ss);
                }
            }

            // Add any new superpowers
            foreach (var sp in updatedSuperpowers)
            {
                if (!superhero.SuperheroSuperpowers.Any(ss => ss.Superpower == sp))
                {
                    var ss = new SuperheroSuperpower { Superhero = superhero, Superpower = sp };
                    superhero.SuperheroSuperpowers.Add(ss);
                }
            }

            _context.SaveChanges();
        }

        public void DeleteSuperhero(int id)
        {
            var heroToDeleteDTO = GetHeroById(id);

            if (heroToDeleteDTO == null)
            {
                throw new ArgumentException("Superhero not found.");
            }

            var heroToDelete = SuperheroDTO.ToSuperhero(heroToDeleteDTO);

            _context.Entry(heroToDelete).State = EntityState.Detached;
            _context.Superheroes.Remove(heroToDelete);

            _context.SaveChanges();
        }

        public List<Superhero> GetSuperheroesByPower(string searchPattern)
        {
            var matchingPowers = _context.Superpowers
                .Where(p => p.Name.Contains(searchPattern) || p.Description.Contains(searchPattern) || p.Category.Contains(searchPattern))
                .Select(p => p.SuperpowerId)
                .ToList();

            var matchingSuperheroes = _context.Superheroes
                
                .ToList();

            return matchingSuperheroes;
        }

    }

    public interface ISuperheroRepository
    {
        IEnumerable<SuperheroDTO> GetAllHeroes();
        SuperheroDTO GetHeroById(int id);
        void AddSuperhero(SuperheroDTO superheroDTO);
        void UpdateSuperhero(SuperheroDTO superheroDTO);
        void DeleteSuperhero(int id);
        List<Superhero> GetSuperheroesByPower(string searchPattern);
    }

}

