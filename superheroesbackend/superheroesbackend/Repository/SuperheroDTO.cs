﻿using superheroesbackend.Models;

namespace superheroesbackend.Repository
{
    public class SuperheroDTO
    {
        public int SuperheroId { get; set; }
        public string? Name { get; set; }
        public string? Gender { get; set; }
        public string? SecretIdentity { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string? HomeLocation { get; set; }
        public string? CurrentLocation { get; set; }
        public string? CostumeDescription { get; set; }
        public string? Weakness { get; set; }
        public string? ImagePath { get; set; }
        public string? InclusiveAge { get; set; }
        // public List<int>? ExistingSuperpowerIds { get; set; } = new List<int>();
        public virtual ICollection<SuperpowerDTO>? Superpowers { get; set; }

        public static Superhero ToSuperhero(SuperheroDTO superheroDTO)
        {
            return new Superhero
            {
                SuperheroId = superheroDTO.SuperheroId,
                Name = superheroDTO.Name,
                Gender = superheroDTO.Gender,
                SecretIdentity = superheroDTO.SecretIdentity,
                DateOfBirth = superheroDTO.DateOfBirth,
                HomeLocation = superheroDTO.HomeLocation,
                CurrentLocation = superheroDTO.CurrentLocation,
                CostumeDescription = superheroDTO.CostumeDescription,
                Weakness = superheroDTO.Weakness,
                ImagePath = superheroDTO.ImagePath,
                InclusiveAge = superheroDTO.InclusiveAge
            };
        }

        public static Superhero SuperheroDTOtoSuperhero(SuperheroDTO superheroDTO, Superhero superhero)
        {
            superhero.SuperheroId = superheroDTO.SuperheroId;
            superhero.Name = superheroDTO.Name;
            superhero.Gender = superheroDTO.Gender;
            superhero.SecretIdentity = superheroDTO.SecretIdentity;
            superhero.DateOfBirth = superheroDTO.DateOfBirth;
            superhero.HomeLocation = superheroDTO.HomeLocation;
            superhero.CurrentLocation = superheroDTO.CurrentLocation;
            superhero.CostumeDescription = superheroDTO.CostumeDescription;
            superhero.Weakness = superheroDTO.Weakness;
            superhero.ImagePath = superheroDTO.ImagePath;
            superhero.InclusiveAge = superheroDTO.InclusiveAge;
            return superhero;
        }

        public static SuperheroDTO FromSuperhero(Superhero superhero)
        {
            return new SuperheroDTO
            {
                SuperheroId = superhero.SuperheroId,
                Name = superhero.Name,
                Gender = superhero.Gender,
                SecretIdentity = superhero.SecretIdentity,
                DateOfBirth = superhero.DateOfBirth,
                HomeLocation = superhero.HomeLocation,
                CurrentLocation = superhero.CurrentLocation,
                CostumeDescription = superhero.CostumeDescription,
                Weakness = superhero.Weakness,
                ImagePath = superhero.ImagePath,
                InclusiveAge = superhero.InclusiveAge
            };
        }
    }


}
