﻿using superheroesbackend.Models;

namespace superheroesbackend.Repository
{
    public class SuperpowerDTO
    {
        public int SuperpowerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public static Superpower ToSuperpower(SuperpowerDTO superpowerDTO)
        {
            return new Superpower
            {
                SuperpowerId = superpowerDTO.SuperpowerId,
                Name = superpowerDTO.Name,
                Description = superpowerDTO.Description,
                Category = superpowerDTO.Category
            };
        }

        public static SuperpowerDTO FromSuperpower(Superpower superpower)
        {
            return new SuperpowerDTO
            {
                SuperpowerId = superpower.SuperpowerId,
                Name = superpower.Name,
                Description = superpower.Description,
                Category = superpower.Category
            };
        }
    }

}
