using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.Net.Http.Headers;
using System.Text;
using superheroesbackend.Models;
using superheroesbackend.Repository;
namespace superheroesbackend
{
    public class Program
    {

        static void Main(string[] args)
        {
            string MyPolicy = "_myPolicy";
            ConfigurationBuilder configbuilder = new ConfigurationBuilder(); configbuilder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfiguration config = configbuilder.Build();

            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddDbContext<SupersContext>(options =>
            {
                options.UseSqlServer(config.GetConnectionString("SuperheroesDb"));
            });

           // builder.Services.AddScoped<SuperheroRepository>();
            builder.Services.AddScoped<ISuperheroRepository, SuperheroRepository>();


            // Add services to the container.
            builder.Services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: "MyPolicy",
                    policy =>
                    {
                        policy.WithOrigins("http://example.com",
                                "http://www.contoso.com",
                                "https://cors1.azurewebsites.net",
                                "https://cors3.azurewebsites.net",
                                "https://localhost:7255",
                                "http://localhost:4200")
                            .WithMethods("PUT", "POST", "DELETE", "GET", "OPTIONS").AllowAnyHeader();
                    });
            });

            var app = builder.Build();

            // Use CORS policy
            app.UseCors(MyPolicy);

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }

    }
}