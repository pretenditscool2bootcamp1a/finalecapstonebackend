﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIprojectTests
{
    public class SuperheroRepositoryTests
    {
        [Fact]
        public void TestAddSuperhero()
        {
            // Arrange
            var context = new ApplicationDbContext(options);
            var repository = new SuperheroRepository(context);

            var superhero = new SuperheroDTO
            {
                Name = "Superman",
                Gender = "Male",
                SecretIdentity = "Clark Kent",
                DateOfBirth = new DateTime(1938, 1, 1),
                HomeLocation = "Metropolis",
                CurrentLocation = "Earth",
                CostumeDescription = "Red cape and blue suit",
                Weakness = "Kryptonite",
                ImagePath = "images/superman.jpg",
                InclusiveAge = "30s",
                Superpowers = new List<SuperpowerDTO>
        {
            new SuperpowerDTO
            {
                Name = "Super Strength",
                Description = "The ability to lift heavy objects",
                Category = "Strength"
            },
            new SuperpowerDTO
            {
                Name = "Flight",
                Description = "The ability to fly through the air",
                Category = "Movement"
            }
        }
            };

            // Act
            repository.AddSuperhero(superhero);

            // Assert
            var addedSuperhero = context.Superheroes.Include(sh => sh.SuperheroSuperpowers)
                                                     .ThenInclude(ss => ss.Superpower)
                                                     .FirstOrDefault(sh => sh.Name == "Superman");

            Assert.NotNull(addedSuperhero);
            Assert.Equal(superhero.Name, addedSuperhero.Name);
            Assert.Equal(superhero.Gender, addedSuperhero.Gender);
            Assert.Equal(superhero.SecretIdentity, addedSuperhero.SecretIdentity);
            Assert.Equal(superhero.DateOfBirth, addedSuperhero.DateOfBirth);
            Assert.Equal(superhero.HomeLocation, addedSuperhero.HomeLocation);
            Assert.Equal(superhero.CurrentLocation, addedSuperhero.CurrentLocation);
            Assert.Equal(superhero.CostumeDescription, addedSuperhero.CostumeDescription);
            Assert.Equal(superhero.Weakness, addedSuperhero.Weakness);
            Assert.Equal(superhero.ImagePath, addedSuperhero.ImagePath);
            Assert.Equal(superhero.InclusiveAge, addedSuperhero.InclusiveAge);
            Assert.Equal(superhero.Superpowers.Count, addedSuperhero.SuperheroSuperpowers.Count);
        }
    }
}
