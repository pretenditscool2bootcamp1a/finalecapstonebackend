﻿using Xunit;


namespace APIprojectTests
{
    public class TestRunner
    {
        [Fact]
        public void RunAllTests()
        {
            // Create instances of each test class
            var superheroRepositoryTests = new SuperheroRepositoryTests();
         //   var superheroControllerTests = new SuperheroControllerTests();

            // Call the test methods on each test class
            superheroRepositoryTests.TestAddSuperhero();
        //    superheroRepositoryTests.TestGetSuperheroById();
        //    superheroControllerTests.TestGetAllSuperheroes();
        //    superheroControllerTests.TestGetSuperheroById();
        //    superheroControllerTests.TestAddSuperhero();
        //    superheroControllerTests.TestUpdateSuperhero();
        //    superheroControllerTests.TestDeleteSuperhero();

            // Run the tests using the test runner
            // Example using Xunit:
            Xunit.Runner.VisualStudio.TestAdapter.VsTestRunner.RunTests(new[] { typeof(TestRunner).Assembly.Location }, null, null);
        }
    }
}

namespace Xunit
{
    class Runner
    {
    }
}