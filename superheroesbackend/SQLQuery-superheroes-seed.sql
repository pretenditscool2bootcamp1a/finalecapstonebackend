﻿INSERT INTO superhero (name, gender, secret_identity, date_of_birth, home_location, current_location, costume_description, weakness, image_path, inclusive_age)
VALUES
('Thunderstrike', 'Male', 'John Parker', '1990-02-14', 'New York', 'New York', 'A blue suit with a white lightning bolt and a hammer emblem', 'Water', '/images/thunderstrike.png', 'Early 30s'),
('Shadowdancer', 'Female', 'Haley Kim', '1985-09-12', 'Los Angeles', 'Los Angeles', 'A black suit with a silver cape and a mask', 'Fire', '/images/shadowdancer.png', 'Mid 30s'),
('Magma Blaze', 'Non-binary', 'Riley Smith', '1993-11-01', 'Dallas', 'Dallas', 'An orange and red suit with a flame emblem', 'Ice', '/images/magma_blaze.png', 'Late 20s'),
('Frostbite', 'Female', 'Evelyn Lee', '1995-04-23', 'Toronto', 'Toronto', 'A white suit with blue accents and an ice crystal emblem', 'Fire', '/images/frostbite.png', 'Mid 20s'),
('Gravity Shift', 'Male', 'Aaron Brown', '1988-06-19', 'San Francisco', 'San Francisco', 'A black and purple suit with a gravitational pull emblem', 'Sound', '/images/gravity_shift.png', 'Early 30s'),
('Nova', 'Non-binary', 'Jamie Kim', '1992-02-18', 'Seoul', 'Seoul', 'A white suit with gold and blue accents and a star emblem', 'Electricity', '/images/nova.png', 'Late 20s'),
('Wildfire', 'Female', 'Chloe Nguyen', '1986-07-09', 'Paris', 'Paris', 'A red suit with orange flames and a phoenix emblem', 'Water', '/images/wildfire.png', 'Mid 30s'),
('Aurora', 'Non-binary', 'Cameron Chen', '1998-09-22', 'Vancouver', 'Vancouver', 'A purple suit with a white cape and an aurora borealis emblem', 'Sound', '/images/aurora.png', 'Early 20s'),
('Eclipse', 'Male', 'Ryan Kim', '1991-03-07', 'Sydney', 'Sydney', 'A black and blue suit with a crescent moon emblem', 'Light', '/images/eclipse.png', 'Early 30s'),
('Tidal Wave', 'Female', 'Kara Singh', '1996-08-31', 'Mumbai', 'Mumbai', 'A blue and green suit with a water wave emblem', 'Electricity', '/images/tidal_wave.png', 'Mid 20s');
